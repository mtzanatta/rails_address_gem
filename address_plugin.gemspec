$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "address_plugin/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "address_plugin"
  s.version     = AddressPlugin::VERSION
  s.authors     = ["marcelo.zanatta"]
  s.email       = ["mtzanatta@gmail.com"]
  s.homepage    = ""
  s.summary     = "Summary of AddressPlugin."
  s.description = "Description of AddressPlugin."
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.rdoc"]
  s.test_files = Dir["test/**/*"]

  s.add_dependency "rails", "~> 4.2.6"

  s.add_development_dependency "sqlite3"
end
