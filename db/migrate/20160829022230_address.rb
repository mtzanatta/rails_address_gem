class Address < ActiveRecord::Migration
    def change

        create_table :addresses do |t|
            t.integer :state_id
            t.integer :city_id
            t.string :neighborhood
            t.string :street
            t.string :cep
            t.string :number
            t.string :complement
            t.string :latitude
            t.string :longitude
            t.text :notes

            t.timestamps null: false
        end

    end
end
