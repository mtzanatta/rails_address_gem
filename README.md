# README #

Esse plugin concentra todos os dados para funcionamento de endereços

### Instalação

    gem 'address_plugin' , :git => 'git@bitbucket.org:mtzanatta/address_plugin.git'
    
* Necessita fazer a importação do banco de dados.
* Deump localizada no projeto.
    
## Uso
    
Funciona baseado em rotas para a API que são chamados via .js do plugin.        
Possui as telas de **formulário** e **show** dos dados. apenas adicionar a chamada ao plugin

    <%= f.fields_for :address do |address| %>
        <%= render :partial => "layouts/address_plugin/form", :locals => {:address => address} %>
    <%end%>

#### Rotas

Adicionar as rotas: 

    namespace :api do
        resources :states
        resources :cities
        resources :neighborhood do
            get 'name', on: :member
        end
        resources :streets do
            get 'name', on: :member
        end
        root :to => "/"
    end

    