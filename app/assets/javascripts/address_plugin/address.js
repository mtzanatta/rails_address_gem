
var ADDRESS = ADDRESS || {};

ADDRESS.commonMethods = {

    //Insere os dados retornado de um unico address
     insert_data: function(results){

        if(results != null && results.state_id != null){
            var state_select = "#address_state_id";
            var city_select = "#address_city_id";

            $(state_select).val(ADDRESS.commonMethods.address_get_state(results));
            $(state_select).trigger("chosen:updated");

            ADDRESS.commonMethods.search_cities();

            $(city_select).val(ADDRESS.commonMethods.address_get_city(results));
            $(city_select).trigger("chosen:updated");

            $("#address_neighborhood_text").val(ADDRESS.commonMethods.address_get_neighborhood(results));
            $("#address_street_text").val(ADDRESS.commonMethods.address_get_street(results));
            $("#address_number_text").val(ADDRESS.commonMethods.address_get_number(results));
            $("#address_complement_text").val(ADDRESS.commonMethods.address_get_complement(results));
            $("#address_note_text").val(ADDRESS.commonMethods.address_get_notes(results));


            $('#div_city').show();
            $('#div_address').show();

        }else
        {
          ADDRESS.commonMethods.hide_campos('state');
        }
    },

     address_get_state: function(address) {
        if (address != null)
            return address.state_id;
        return null;
    },

     address_get_city: function(address) {
        if (address != null)
            return address.city_id;
        return null;
    },

     address_get_neighborhood: function(address) {
        if (address != null)
            return address.neighborhood;
        return null;
    },

     address_get_street: function(address) {
        if (address != null)
            return address.street;
        return null;
    },

    //Return cep of street if exists
    //Else return cep of city
     address_get_cep: function(address) {
        if (address != null)
            return address.cep;
        return null;
    },

    address_get_number: function(address) {
        if (address != null)
            return address.number;
        return null;
    },

    address_get_complement: function(address) {
        if (address != null)
            return address.complement;
        return null;
    },

    address_get_notes: function(address) {
        if (address != null)
            return address.notes;
        return null;
    },

     //Esconde os campos
    hide_campos: function(tipo)
    {
        if(tipo == "state"){
            $('#div_city').hide();
            $('#address_city_select').find('option')
                .remove()
                .end()
                .append('<option value="">Selecione</option>')
                .val('');
        }

        if(tipo == "state" || tipo == "city"){
            $('#div_address').hide();
            $('#address_neighborhood_text').val("");
            $('#address_street_text').val("");
            $('#address_number_text').val("");
            $('#address_complement_text').val("");
            $('#address_note_text').val("");
        }
    },

    //Buscas todos os states via Ajax
    get_ajax: function(url){
        var ret = null;
        $.ajax({
            type: "GET",
            async: false,
            dataType: "json",
            url: url,
            success: function(data) {
              ret = data;
            }
        });
        return ret;
    },

    //Buscas todos os states via Ajax
    get_all_states: function(){
      ADDRESS.commonMethods.insert_option("address_state",
      ADDRESS.commonMethods.get_ajax("/address_plugin/states?country_id=1"));
    },

    insert_option: function(select,values)
    {
        var select_option = "#"+select+"_id";
        var select_chose = select_option + "_chosen";

        $(select_option).find('option')
            .remove()
            .end()
            .append('<option value="">Selecione</option>')
            .val('');

        if(values != null){
            $.each(values, function(text, item) {
                $(select_option).append(new Option(item.name, item.id));
            });
            $(select_option)
                .find('option:first-child').prop('selected', true)
                .end().trigger('chosen:updated');
        }
    },

    search_cities: function()
    {
        var state_id = $('#address_state_id').val();
        ADDRESS.commonMethods.insert_option("address_city",
        ADDRESS.commonMethods.get_ajax("/address_plugin/cities?state_id="+state_id ));
    },

    set_state_by_id : function(state_id){
        state_select = "#address_state_id";
        $(state_select).val(state_id);
        $(state_select).trigger("chosen:updated");
        ADDRESS.commonMethods.hide_campos("state");
        ADDRESS.commonMethods.search_cities();
        $('#div_city').show();
    },
};

(function() {
    $('.chosen').chosen({
        no_results_text: "Ops! Nenhum resultado encontrado!",
    	  allow_single_deselect: true
    });

    //Busca um address pelo CEP
    $('#search_cep').click(function(){
      ADDRESS.commonMethods.insert_data(ADDRESS.commonMethods.get_ajax( "/address_plugin/addresses?cep=" +
      $('#address_cep').val().replace(".","")));
    });

    $('#address_state_id').chosen().change(function() {
        ADDRESS.commonMethods.hide_campos("state");
        ADDRESS.commonMethods.search_cities();
        $('#div_city').show();
    });

     $('#address_city_id').chosen().change(function() {
        var city_id = $('#address_city_id').val();
        $('#div_address').show();
     });

    //Carrega todos os states
    ADDRESS.commonMethods.get_all_states();
    ADDRESS.commonMethods.hide_campos('state');
})();
