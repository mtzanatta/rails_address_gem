#encoding: utf-8
module AddressPlugin
    class ApplicationController  < ActionController::Base

        # helper AddressPlugin::AddressesHelper

        def return_error
            render :json => @error_object.to_json, status: :not_found
        end
    end
end
