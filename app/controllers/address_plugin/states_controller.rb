#encoding: utf-8
module AddressPlugin

    class StatesController < ApplicationController

        def index
            if params[:country_id]
                @states = State.where(country_id: params[:country_id]).order(:name)
                if @states and @states.count > 0
                    render json: @states and return
                end
                return_error() and return
            end
            return_error() and return
        end

        def show
            if params[:id] and not params[:id].empty?
                @state = State.find(params[:id])
                if @state
                    render json: @state and return
                end
                return_error() and return
            end
            return_error() and return
        end

    end
end
