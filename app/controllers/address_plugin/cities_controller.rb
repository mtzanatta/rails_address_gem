#encoding: utf-8
module AddressPlugin

    class CitiesController < ApplicationController

        def index
            if params[:state_id]
                @cities = City.where(state_id: params[:state_id])
                if @cities and @cities.count > 0
                    render json: @cities and return
                end
                return_error() and return
            end
            return_error() and return
        end

        def show
            if params[:id] and not params[:id].empty?
                @city = City.find(params[:id])
                if @city
                    render json: @city and return
                end
                return_error() and return
            end
            return_error() and return
        end

    end
end
