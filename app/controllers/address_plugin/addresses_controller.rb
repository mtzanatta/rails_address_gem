#encoding: utf-8
module AddressPlugin
    class AddressesController < ApplicationController

        def index
            if params[:cep] != nil and  not params[:cep].empty?

                #Busca o CEP em logradouro
                @address = search_cep(
                nil,
                params[:cep],
                nil,
                nil
                )

                if @address
                    render json: @address and return
                end
                return_error() and return
            end
            return_error() and return
        end

        private
        #Retorna o endereco apartir do cep
        def search_cep(address,cep,name_neighborhood,name_street)

            street = Street.where(cep: cep.gsub(".","")).first

            if street

                address = Address.new
                address.city_id = street.city_id
                address.state_id = street.city.state_id
                if not street.neighborhood.nil?
                    address.neighborhood = street.neighborhood.name || name_neighborhood
                end
                address.street = street.complement || name_street
                address.cep = street.cep
            else
                city = City.where(cep: cep.gsub(".","")).first
                if city
                    address = Address.new
                    address.city_id = city.id
                    address.state_id = city.state_id
                end
            end

            return address
        end

    end
end
