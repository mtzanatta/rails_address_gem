#encoding: utf-8
module AddressPlugin
    class NeighborhoodsController < ApplicationController
        def index
            if params[:city_id]
                @neighborhoods = Neighborhood.where(city_id: params[:city_id])
                if @neighborhoods
                    render json: @neighborhoods and return
                end
                return_error() and return
            end
            return_error() and return
        end

        def show
            if params[:id] and not params[:id].empty?
                @neighborhood = Neighborhood.find(params[:id])
                if @neighborhood
                    render json: @neighborhood and return
                end
                return_error() and return
            end
            return_error() and return
        end

        def name
            if params[:name] and params[:id]
                @neighborhoods = Neighborhood.where("city_id = #{params[:id]} and name like '%#{params[:name]}%' ")
            end

            if @neighborhoods
                render json: @neighborhoods and return
            end

            return_error() and return
        end
    end
end
