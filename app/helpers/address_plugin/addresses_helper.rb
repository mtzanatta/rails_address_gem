#encoding: utf-8
# require address

module AddressPlugin
    module AddressesHelper

        def address_get_states
            return State.where(:country_id => 1).collect{ |p| [ p.name, p.id ] }
        end

        #{address_get_street(address)}, N: #{address.number}, #{address_get_state_initial(address)} - #{address_get_city(address)} - #{address_get_neighborhood(address)}, CEP: #{address_get_cep(address)} #{address.complemento != nil ? ", "+ address.complemento : nil }
        def address_get_address_simples(address)
            if address and address.street and not address.street.empty?
                return "#{address_get_street(address)}, N: #{address.number}, #{address_get_state_initials(address)} - #{address_get_city(address)} - #{address_get_neighborhood(address)}, CEP: #{address_get_cep(address)} #{address.complement != nil ? ", "+ address.complement : nil }"
            end

            return nil
        end

        #Retorna o nome do estado
        def address_get_state_name(object_class)
            address = get_instance(object_class)
            if address and address.state
                return address.state.name
            end
            return nil
        end

        #Retorna a sigla do estado
        def address_get_state_initial(object_class)
            address = get_instance(object_class)
            if address and address.state
                return address.state.initial
            end
            return nil
        end

        #Retorna o nome da cidade
        def address_get_city_name(object_class)
            address = get_instance(object_class)
            if address and address.city
                return address.city.name
            end
            return nil
        end

        def address_get_neighborhoods(id)
            if id != nil
                return Neighborhood.where(id: id).collect {|p| [ p.name, p.id] }
            end
        end

        #Apartir do local de atendimento
        def address_get_neighborhood_name(object_class)
            address = get_instance(object_class)
            if address
                return address.neighborhood
            end
            return nil
        end

        #Apartir do local de atendimento
        def address_get_street(object_class)
            address = get_instance(object_class)
            if address
                return address.street
            end
            return nil
        end

        #Retorna o cep de street se street existir
        #Se nao existir traz o cep de city
        def address_get_cep(object_class)
            address = get_instance(object_class)
            if address
                return address.cep
            end
            return nil
        end

        def address_get_number(object_class)
            address = get_instance(object_class)
            if address
                return address.number
            end
        end

        private

        #Retorna true se for uma instancia de address
        def get_instance(object_class)
            if object_class and object_class.instance_of? AddressPlugin::Address
                return object_class
            elsif object_class
                return object_class.address
            end
            return nil
        end

    end
end
