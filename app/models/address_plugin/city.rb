module AddressPlugin
    class City < ActiveRecord::Base
        self.table_name = "cities"

        belongs_to :state
    end
end
