module AddressPlugin
    class Address < ActiveRecord::Base

        self.table_name = "addresses"

        belongs_to :country
        belongs_to :state
        belongs_to :city

    end
end
