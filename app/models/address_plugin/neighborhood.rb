module AddressPlugin
    class Neighborhood < ActiveRecord::Base
        self.table_name = "neighborhoods"

        belongs_to :city
    end
end
