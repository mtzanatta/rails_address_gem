module AddressPlugin
    class Street < ActiveRecord::Base
        self.table_name = "streets"

        belongs_to :city
        belongs_to :neighborhood
    end
end
