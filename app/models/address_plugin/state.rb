module AddressPlugin
    class State < ActiveRecord::Base
        self.table_name = "states"

        belongs_to :country
    end
end
