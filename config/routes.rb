Rails.application.routes.draw do
    #
    namespace :address_plugin do
        resources :addresses
        resources :states
        resources :cities
        resources :neighborhood do
            get 'name', on: :member
        end
        resources :streets do
            get 'name', on: :member
        end
    end

end
